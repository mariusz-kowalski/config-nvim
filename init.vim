" Install vim-plug
" ---
" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
"    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" ---
" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
Plug 'terryma/vim-multiple-cursors'
Plug 'bling/vim-airline'
Plug 'junegunn/fzf', { 'do': './install --bin' }
Plug 'junegunn/fzf.vim'
Plug 'lnl7/vim-nix'
Plug 'lambdalisue/suda.vim'
Plug 'habamax/vim-asciidoctor'
Plug 'scrooloose/nerdcommenter'
" Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'marcweber/vim-addon-local-vimrc'
Plug 'scrooloose/syntastic'
Plug 'kabbamine/vcoolor.vim'
Plug 'tpope/vim-fugitive'
Plug 'rust-lang/rust.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

:filetype plugin on

" options
" margins
set colorcolumn=81,101,102,121,122,123
highlight ColorColumn ctermbg=235
" spell
" set spell
" line numbers
set number
highlight LineNr ctermfg=242 ctermbg=234
" system clipboard
set clipboard+=unnamedplus
" smart home
noremap <expr> <silent> <Home> col('.') == match(getline('.'),'\S')+1 ? '0' : '^'
imap <silent> <Home> <C-O><Home>
" 2 spaces for tab
set expandtab
set shiftwidth=2
" show full file path
let g:airline_section_c='%{resolve(expand("%:p"))}'
" insert mode background color
highlight Normal ctermbg=235
:au InsertLeave * hi Normal term=reverse ctermbg=235
:au InsertEnter * hi Normal term=NONE    ctermbg=NONE

" remove trailing spaces before write
autocmd BufWritePre * %s/\s\+$//e

""" suda
let g:suda_smart_edit = 1


""" nerdcommenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

""" nerd tree
map <C-\> :NERDTreeToggle<CR>
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

""" fzf
nnoremap <C-b> :Buffers<CR>
nnoremap <C-g>g :Rg<CR>
nnoremap <leader><leader> :Commands<CR>
nnoremap <C-p> :Files<CR>
let g:fzf_preview_window = 'up:70%'
let g:fzf_layout = {'left': '100%'}

""" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" let g:syntastic_ruby_checkers = ['mri', 'rubocop']
